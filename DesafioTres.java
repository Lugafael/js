import java.util.Random;

public class DesafioTres {

	public static void main(String[] args) {
		
		Random random = new Random();
		
		String Robo1 = " ";
		String Robo2 = " ";
		
		int[] cartas = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
		                                          //J //Q //K //A
		String[] naipes = {"C", "E", "O", "P"};
		String win = " ";
		
		//Mão Robô Um
		int[] roboOneC = new int[5];
		String[] roboOneN = new String[5];
		int first = random.nextInt(13);
		int firstOne = random.nextInt(3);
		roboOneC[0] = cartas[first];
		roboOneN[0] = naipes[firstOne];
		
		//Mão Robô Dois
		int[] roboTwoC = new int[5];
		String[] roboTwoN = new String[5];
		int firstt = random.nextInt(13);
		int firstTwo = random.nextInt(3);
		roboTwoC[0] = cartas[firstt];
		roboTwoN[0] = naipes[firstTwo];
		
		for(int i = 1; i < 3; i++) {
			
			int cards = random.nextInt(12);
			int naip = random.nextInt(3);
			roboOneC[i] = cartas[cards];
			if(roboOneC[i] == roboOneC[i - 1] && roboOneN[i] != roboOneN[i - 1]) {
				roboOneN[i] = naipes[naip];
			}else {
				roboOneN[i] = naipes[naip];
			}
		}
		
        for(int i = 1; i < 3; i++) {
			
			int cards = random.nextInt(12);
			int naip = random.nextInt(3);
			roboTwoC[i] = cartas[cards];
			if(roboTwoC[i] == roboTwoC[i - 1]) {
				if(roboTwoN[i] != roboTwoN[i - 1]) {
					roboTwoN[i] = naipes[naip];
				}
			}else {
				roboTwoN[i] = naipes[naip];
			}
			
		}
        
        for(int d = 3; d < 5; d++) {
        	int cards = random.nextInt(12);
			int naip = random.nextInt(3);
			roboOneC[d] = cartas[cards];
			roboOneN[d] = naipes[naip];
			roboTwoC[d] = cartas[cards];
			roboTwoN[d] = naipes[naip];
        }        
        
        //Carta Alta
        int aux = 0;
        int auxx = 0;
        
        for(int i = 0; i < roboOneC.length; i++) {
        	for(int c = 0; c < roboOneC.length; c++) {
        		if(roboOneC[i] >= roboOneC[c] && roboOneC[i] >= aux) {
        			aux = roboOneC[i];
        		}
        	}
        }
                
        for(int i = 0; i < roboTwoC.length; i++) {
        	for(int c = 0; c < roboTwoC.length; c++) {
        		if(roboTwoC[i] >= roboTwoC[c]) {
        			auxx = roboTwoC[i];
        		}
        	}
        }
        
        if(aux > auxx) {
        	Robo1 = "Carta Alta";
        	Robo2 = "Sem Jogada";
        }else if(auxx > aux) {
        	Robo2 = "Carta Alta";
        	Robo1 = "Sem Jogada";
        }else if(aux == auxx) {
        	Robo1 = "Carta Alta";
        	Robo2 = "Carta Alta";
        }
     
        //Par 
        int contOnePar = 0;
        
        for(int z = 0; z < roboOneC.length; z++) {
        	for(int x = 0; x < roboOneC.length; x++) {
        		if(roboOneC[z] == roboOneC[x]) {
        			contOnePar++;
        		}
        	}
        }
        
        if(contOnePar == 7) {
        	Robo1 = "Par";
        }
        
        int contTwoPar = 0;
        
        for(int z = 0; z < roboTwoC.length; z++) {
        	for(int x = 0; x < roboTwoC.length; x++) {
        		if(roboTwoC[z] == roboTwoC[x]) {
        			contTwoPar++;
        		}
        	}
        }
        
        if(contTwoPar == 7) {
        	Robo2 = "Par";
        }
        
        //Dois Pares
        int contOneDois = 0;
        
        for(int x = 0; x < roboOneC.length; x++) {
        	for(int v = 0; v < roboOneC.length; v++) {
        		if(roboOneC[x] == roboOneC[v]) {
        			contOneDois++;
        		}
        	}
        }
        
        if(contOneDois == 9) {
        	Robo1 = "Dois Pares";
        }
        
        int contTwoDois = 0;
        
        for(int z = 0; z < roboTwoC.length; z++) {
        	for(int x = 1; x < roboTwoC.length; x++) {
        		if(roboTwoC[z] == roboTwoC[x]) {
        			contTwoDois++;
        		}
        	}
        }
        
        if(contTwoDois == 7) {
        	Robo2 = "Dois Pares";
        }
        
        //Trio
        int contTrioOneC = 0;
        
        for(int x = 0; x < roboOneC.length; x++) {
        	for(int v = 0; v < roboOneC.length; v++) {
        		if(roboOneC[x] == roboOneC[v]) {
        			contTrioOneC++;
        		}
        	}
        }
        
        if(contTrioOneC == 11) {
        	Robo1 = "Trio";
        }
        
        int contTrioTwoC = 0;
        
        for(int x = 0; x < roboTwoC.length; x++) {
        	for(int v = 0; v < roboTwoC.length; v++) {
        		if(roboTwoC[x] == roboTwoC[v]) {
        			contTrioTwoC++;
        		}
        	}
        }
        
        if(contTrioTwoC == 11) {
        	Robo2 = "Trio";
        }
        
        //Straight
        int contStraightOne = 0;
        int contStraightOneN = 0;
        
        for(int i = 0; i < roboOneC.length; i++) {
    	   for(int n = 0; n < roboOneC.length; n++) {
    		   if(roboOneC[n] + 1 == roboOneC[i] || roboOneC[n] - 1 == roboOneC[i]) {
    			   contStraightOne++;
    		   }
    	   }
        }
        
        for(int i = 0; i < roboOneN.length-1; i++) {
        	if(roboOneN[i] != roboOneN[i + 1]) {
        		contStraightOneN++;
        	}
        }
        
        if(contStraightOne == 8 && contStraightOneN >= 1) {
        	Robo1 = "Straight";
        }
        
        int contStraightTwo = 0;
        int contStraightTwoN = 0;
        
        for(int i = 0; i < roboTwoC.length; i++) {
    	   for(int n = 0; n < roboTwoC.length; n++) {
    		   if(roboTwoC[n] + 1 == roboTwoC[i] || roboTwoC[n] - 1 == roboTwoC[i]) {
    			   contStraightTwo++;
    		   }
    	   }
        }
        
        for(int i = 0; i < roboTwoN.length-1; i++) {
        	if(roboTwoN[i] != roboTwoN[i + 1]) {
        		contStraightTwoN++;
        	}
        }
        
        if(contStraightTwo == 8 && contStraightTwoN >= 1) {
        	Robo2 = "Straight";
        }
        
        //Flush
        int contOneNFlush = 0;
        int contOneCFlush = 0;
        
        for(int i = 0; i < roboOneN.length; i++) {
        	for(int b = 0; b < roboOneN.length; b++) {
        		if(roboOneN[i] == roboOneN[b]) {
        			contOneNFlush++;
        		}
        	}
        }
        
        for(int i = 0; i < roboOneC.length - 1; i++) {
        	if(roboOneC[i + 1] + 1 != roboOneC[i] || roboOneC[i + 1] - 1 != roboOneC[i]) {
        		contOneCFlush++;
        	}
        }
        
        if(contOneNFlush == 25 && contOneCFlush >= 1) {
        	Robo1 = "Flush";
        }
        
        int contTwoNFlush = 0;
        int contTwoCFlush = 0;
        
        for(int i = 0; i < roboTwoN.length; i++) {
        	for(int b = 0; b < roboTwoN.length; b++) {
        		if(roboTwoN[i] == roboTwoN[b]) {
        			contTwoNFlush++;
        		}
        	}
        }
        
        for(int i = 0; i < roboTwoC.length - 1; i++) {
        	if(roboTwoC[i + 1] + 1 != roboTwoC[i] || roboTwoC[i + 1] - 1 != roboTwoC[i]) {
        		contTwoCFlush++;
        	}
        }
        
        if(contTwoNFlush == 25 && contTwoCFlush >= 1) {
        	Robo2 = "Flush";
        }
        
        //Full House
        int contHouse = 0;
        int contFull = 0;
        
        for(int i = 0; i < roboOneC.length; i++) {
        	for(int p = 0; p < roboOneC.length; p++) {
        		if(roboOneC[i] == roboOneC[p]) {
        			contHouse++;
        		}
        		if(roboOneC[i] != roboOneC[p]) {
        			contFull++;
        		}
        	}
        }
        
        if(contHouse == 13 && contFull == 12) {
        	Robo1 = "Full House";
        }
        
        int contHousee = 0;
        int contFulll = 0;
        
        for(int i = 0; i < roboTwoC.length; i++) {
        	for(int p = 0; p < roboTwoC.length; p++) {
        		if(roboTwoC[i] == roboTwoC[p]) {
        			contHousee++;
        		}
        		if(roboTwoC[i] != roboTwoC[p]) {
        			contFulll++;
        		}
        	}
        }
        
        if(contHousee == 13 && contFulll == 12) {
        	Robo2 = "Full House";
        }
        
        //Quadra
        boolean quadra = false;
        int contOneC = 0;
        
        for(int i = 0; i < roboOneC.length; i++) {
        	for(int b = 0; b < roboOneC.length; b++) {
        		if(roboOneC[i] == roboOneC[b]) {
        			contOneC++;
        		}
        		
        		if(contOneC == 17) {
        			quadra = true;
        		}
        	}
        }
        
        if(quadra == true) {
        	Robo1 = "Quadra";
        }
        
        boolean quadraa = false;
        int contTwoC = 0;
        
        for(int i = 0; i < roboTwoC.length; i++) {
        	for(int b = 0; b < roboTwoC.length; b++) {
        		if(roboTwoC[i] == roboTwoC[b]) {
        			contTwoC++;
        		}
        		
        		if(contTwoC == 17) {
        			quadraa = true;
        		}
        	}
        }
        
        if(quadraa == true) {
        	Robo2 = "Quadra";
        }
        
        //Straight Flush
        int contSflush = 0;
        int contflushS = 0;
        int flushNaip = 0;
        
        for(int i = 0; i < roboOneC.length; i++) {
        	for(int f = 0; f < roboOneC.length; f++) {
        		int a = roboOneC[f];
        		if(roboOneC[i] == a - 1) {
        			contSflush++;
        		}else if(roboOneC[i] == a + 1) {
        			contflushS++;
        		}
        	}
        }
        
        for(int f = 0; f < 4; f++) {
        	if(roboOneN[f] == roboOneN[f + 1]) {
        		flushNaip++;
        	}
        }
        
        if(contSflush == 4 || contflushS == 4 && flushNaip == 4) {
        	Robo1 = "Straight Flush";
        }
        
        int contSflushh = 0;
        int contflushSs = 0;
        int flushNaipp = 0;
        
        for(int i = 0; i < roboTwoC.length; i++) {
        	for(int f = 0; f < roboTwoC.length; f++) {
        		int a = roboTwoC[f];
        		if(roboTwoC[i] == a - 1) {
        			contSflushh++;
        		}else if(roboTwoC[i] == a + 1) {
        			contflushSs++;
        		}
        	}
        }
        
        for(int f = 0; f < 4; f++) {
        	if(roboTwoN[f] == roboTwoN[f + 1]) {
        		flushNaipp++;
        	}
        }
        
        if(contSflushh == 4 || contflushSs == 4 && flushNaipp == 4) {
        	Robo2 = "Straight Flush";
        }
        
        //Royal Flush
        int royalContN = 0;
        int royalCont = 0;
        
        for(int i = 0; i < roboOneC.length; i++) {
        	if(roboOneC[i] == 14 || roboOneC[i] == 13 || roboOneC[i] == 12 || roboOneC[i] == 11 || roboOneC[i] == 10) {
        		royalCont++;
        	}
        }
        
        for(int g = 0; g < 4; g++) {
        	if(roboOneN[g] == roboOneN[g + 1]) {
        		royalContN++;
        	}
        }
        
        if(royalCont == 5 && royalContN == 4) {
        	Robo1 = "Royal Flush";
        }
        
        int royalContNN = 0;
        int royalContt = 0;
        
        for(int i = 0; i < roboTwoC.length; i++) {
        	if(roboTwoC[i] == 14 || roboTwoC[i] == 13 || roboTwoC[i] == 12 || roboTwoC[i] == 11 || roboTwoC[i] == 10) {
        		royalContt++;
        	}
        }
        
        for(int g = 0; g < 4; g++) {
        	if(roboTwoN[g] == roboTwoN[g + 1]) {
        		royalContNN++;
        	}
        }
        
        if(royalContt == 5 && royalContNN == 4) {
        	Robo2 = "Royal Flush";
        }
        
        //Conversão
        String[] exibicaoRoboOne = new String[5];
        String[] exibicaoRoboTwo = new String[5];
        String Aux = " ";
        
        for(int i = 0; i < roboOneC.length; i++) {
        	if(roboOneC[i] == 14) {
        		Aux = "A";
        		exibicaoRoboOne[i] = Aux + roboOneN[i];
        	}else if(roboOneC[i] == 13) {
        		Aux = "K";
        		exibicaoRoboOne[i] = Aux + roboOneN[i];
        	}else if(roboOneC[i] == 12) {
        		Aux = "Q";
        		exibicaoRoboOne[i] = Aux + roboOneN[i];
        	}else if(roboOneC[i] == 11) {
        		Aux = "J";
        		exibicaoRoboOne[i] = Aux + roboOneN[i];
        	}else {
        		exibicaoRoboOne[i] = roboOneC[i] + roboOneN[i];
        	}
        }
        
        for(int i = 0; i < roboTwoC.length; i++) {
        	if(roboTwoC[i] == 14) {
        		Aux = "A";
        		exibicaoRoboTwo[i] = Aux + roboTwoN[i];
        	}else if(roboTwoC[i] == 13) {
        		Aux = "K";
        		exibicaoRoboTwo[i] = Aux + roboTwoN[i];
        	}else if(roboTwoC[i] == 12) {
        		Aux = "Q";
        		exibicaoRoboTwo[i] = Aux + roboTwoN[i];
        	}else if(roboTwoC[i] == 11) {
        		Aux = "J";
        		exibicaoRoboTwo[i] = Aux + roboTwoN[i];
        	}else {
        		exibicaoRoboTwo[i] = roboTwoC[i] + roboTwoN[i];
        	}
        }
        
        //Condição de Vitória
         
        if(Robo1 == "Royal Flush" && Robo2 != "Royal Flush") {
        	win = "Robô 1 venceu!";
        }else if(Robo1 != "Royal Flush" && Robo2 == "Royal Flush") {
        	win = "Robô 2 venceu!";
        }else if(Robo1 == "Royal Flush" && Robo2 == "Royal Flush") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Straight Flush" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush") {
        	win = "Robô 1 venceu!";
        }else if(Robo1 != "Straight Flush" && Robo2 == "Straight Flush" && Robo1 != "Royal Flush") {
        	win = "Robô 2 venceu!";
        }else if(Robo1 == "Straight Flush" && Robo2 == "Straight Flush") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Quadra") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Quadra") {
        	win = "Robô 2 venceu!";
        }else if(Robo1 == "Quadra" && Robo2 == "Quadra") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Full House") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Full House") {
        	win = "Robô 2 venceu!";
        }else if(Robo1 == "Full House" && Robo2 == "Full House") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Flush" && Robo2 != "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Flush") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Flush" && Robo1 != "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Flush"){
        	win = "Robô 2 venceu!";
        }else if(Robo1 == "Flush" && Robo2 == "Flush") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Straight" && Robo2 != "Flush" && Robo2 != "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Straight") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Straight" && Robo1 != "Flush" && Robo1 != "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Straight") {
        	win = "Robô 2 venceu!";
        }else if(Robo1 == "Straight" && Robo2 == "Straight") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Trio" && Robo2 != "Straight" && Robo2 != "Flush" && Robo2 != "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Trio") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Trio" && Robo1 != "Straight" && Robo1 != "Flush" && Robo1 != "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Trio") {
        	win = "Robô 2 venceu!";
        }else if (Robo1 == "Trio" && Robo2 == "Trio") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Dois Pares" && Robo2 != "Trio" && Robo2 != "Straight" && Robo2 != "Flush" && Robo2 != "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Dois Pares") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Dois Pares" && Robo1 != "Trio" && Robo1 != "Straight" && Robo1 != "Flush" && Robo1 != "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Dois Pares") {
        	win = "Robô 2 venceu!";
        }else if (Robo1 == "Dois Pares" && Robo2 == "Dois Pares") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Par" && Robo2 != "Dois Pares" && Robo2 != "Trio" && Robo2 != "Straight" && Robo2 != "Flush" && Robo2 != "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Par") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Par" && Robo1 != "Dois Pares" && Robo1 != "Trio" && Robo1 != "Straight" && Robo1 != "Flush" && Robo1 != "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Par") {
        	win = "Robô 2 venceu!";
        }else if (Robo1 == "Par" && Robo2 == "Par") {
        	win = "Empate!";
        }
        
        if(Robo1 == "Carta Alta" && Robo2 != "Par" && Robo2 != "Dois Pares" && Robo2 != "Trio" && Robo2 != "Straight" && Robo2 != "Flush" && Robo2 != "Full House" && Robo2 != "Quadra" && Robo2 != "Straight Flush" && Robo2 != "Royal Flush" && Robo2 != "Carta Alta") {
        	win = "Robô 1 venceu!";
        }else if(Robo2 == "Carta Alta" && Robo1 != "Par" && Robo1 != "Dois Pares" && Robo1 != "Trio" && Robo1 != "Straight" && Robo1 != "Flush" && Robo1 != "Full House" && Robo1 != "Quadra" && Robo1 != "Straight Flush" && Robo1 != "Royal Flush" && Robo1 != "Carta Alta") {
        	win = "Robô 2 venceu!";
        }else if (Robo1 == "Carta Alta" && Robo2 == "Carta Alta") {
        	win = "Empate!";
        }
        
        System.out.println("{ Robô 1: " + Robo1 + " (" + exibicaoRoboOne[0] + ", " + exibicaoRoboOne[1] + ", " + exibicaoRoboOne[2] + ", " + exibicaoRoboOne[3] + ", " + exibicaoRoboOne[4] + "), Robô 2: " + Robo2 + " (" + exibicaoRoboTwo[0] + ", " + exibicaoRoboTwo[1] + ", " + exibicaoRoboTwo[2] + ", " + exibicaoRoboTwo[3] + ", " + exibicaoRoboTwo[4] + "), " + win + " }");
        
	}
	
}