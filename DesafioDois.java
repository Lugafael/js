import java.util.Scanner;
import java.util.Random;

public class DesafioDois {
	
	public static void main(String[] args){
		
		Scanner dado = new Scanner(System.in);
		
		String entrada = dado.nextLine();
		String[] dadosSeparados = entrada.split("[\\W]");
		
		int cont = 0;
		int[] X = new int[4];
		
		for(int i = 1; i < dadosSeparados.length; i++) {
			X[i] = Integer.parseInt(dadosSeparados[i]);
		}
		
		int aux = X[1];
		
		if(X[2] > X[1] || X[3] > X[1]) {
			System.out.println("Valor Inválido");
		}else {
			int[][] constelacao = new int[aux][aux];
			
			montarConstelacao(constelacao, aux, aux);
			show(constelacao, aux, aux);
			
			if(constelacao[X[2]][X[3]] == 1) {
				cont++;
			}
			
			if(constelacao[X[3]][X[2]] == 1) {
				cont++;
			}
					
			if(cont >= 2) {
				System.out.println(", Há ligação");
			}else {
				System.out.println(", Não há ligação");
			}
		}
		
		dado.close();
		
	}
	
	public static void montarConstelacao(int[][] arr, int linhas, int colunas) {
		for(int l = 0; l < linhas; l++) {
			for(int c = 0; c < colunas; c++) {
				arr[l][c] = new Random().nextInt(2);
			}
		}
	}
	
	public static void show(int[][] arr, int linhas, int colunas) {
		System.out.println("[");
		System.out.println();
		for(int l = 0; l < linhas; l++) {
			System.out.print("[");
			for(int c = 0; c < colunas; c++) {
				System.out.print(" " + arr[l][c] + " ");
			}
			System.out.print("], ");
			System.out.println();
			System.out.printf("%n");
		}
		System.out.print("]");
	}
	
}
