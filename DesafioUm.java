import java.util.Scanner;

public class DesafioUm {
	
public static void main(String[] args) {
		
		Scanner dado = new Scanner(System.in);
		
		String romano = dado.next();
		
		String b = " ";
		boolean v = false;
		int c = 0;
		char[] array = romano.toCharArray();
		
		for(int i = 0; i < array.length; i++) {
			
			char aux;
			
			aux = array[i];
			b = String.valueOf(aux);
			
			if(b.equals("I")) {
				c = c + 1;
			}
			else if(b.equals("V")) {
				c = c + 5;
			}
			else if(b.equals("X")) {
				c = c + 10;
			}
			else if(b.equals("L")) {
				c = c + 50;
			}
			else if(b.equals("C")) {
				c = c + 100;
			}
			else if(b.equals("D")) {
				c = c + 500;
			}
			else if(b.equals("M")) {
				c = c + 1000;
			}else {
				c = 0;
				v = true;
			}
			

		}
		if(c != 0 && v != true) {
			System.out.println(c);
		}else {
			System.out.println("Número Inválido");
		}
		
		dado.close();
	}
}
